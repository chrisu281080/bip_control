#!/usr/bin/env python3

import struct
import time
import sys
import argparse
from Crypto.Cipher import AES
from bluepy.btle import Peripheral, DefaultDelegate, ADDR_TYPE_RANDOM, ADDR_TYPE_PUBLIC

# For get_time
from datetime import datetime, timedelta

''' TODO
Key should be generated and stored during init
'''


### UUIDs
UUID_SVC_MIBAND1 = "0000fee000001000800000805f9b34fb"
UUID_SVC_MIBAND2 = "0000fee100001000800000805f9b34fb"
UUID_CHAR_AUTH = "00000009-0000-3512-2118-0009af100700"
UUID_SVC_ALERT = "0000180200001000800000805f9b34fb"
UUID_CHAR_ALERT = "00002a0600001000800000805f9b34fb"
UUID_SVC_HEART_RATE = "0000180d00001000800000805f9b34fb"
UUID_CHAR_HRM_MEASURE = "00002a3700001000800000805f9b34fb"
UUID_CHAR_HRM_CONTROL = "00002a3900001000800000805f9b34fb"


HRM_COMMAND = 0x15
HRM_MODE_SLEEP      = 0x00
HRM_MODE_CONTINUOUS = 0x01
HRM_MODE_ONE_SHOT   = 0x02

CCCD_UUID = 0x2902

BASE = "0000%s-0000-1000-8000-00805f9b34fb"
UUID_SERVICE_DEVICE_INFO = BASE % '180a'
UUID_CHARACTERISTIC_SW_REVISION = 0x2a28
UUID_CHARACTERISTIC_HRDW_REVISION = 0x2a27
UUID_CHARACTERISTIC_SERIAL = 0x2a25
UUID_CHARACTERISTIC_CURRENT_TIME = BASE % '2A2B'
UUID_CHARACTERISTIC_NEW_ALERT = BASE % '2A46'
UUID_CHARACTERISTIC_STEPS = "00000007-0000-3512-2118-0009af100700"


class MiBand2(Peripheral):

	def __init__(self, addr, device_key=''):
		try:
			Peripheral.__init__(self, addr, addrType=ADDR_TYPE_RANDOM)
		except:
			Peripheral.__init__(self, addr, addrType=ADDR_TYPE_PUBLIC)

		if len(device_key) > 0:
			self._KEY = bytes.fromhex(device_key)
			self._send_key_cmd = struct.pack('<18s', b'\x01\x00' + self._KEY)
			self._send_rnd_cmd = struct.pack('<2s', b'\x02\x00')
			self._send_enc_key = struct.pack('<2s', b'\x03\x00')
		else:
			self._KEY = b'\x31\x32\x32\x33\x34\x35\x36\x37\x38\x39\x40\x41\x42\x43\x44\x45'
			self._send_key_cmd = struct.pack('<18s', b'\x01\x08' + self._KEY)
			self._send_rnd_cmd = struct.pack('<2s', b'\x02\x08')
			self._send_enc_key = struct.pack('<2s', b'\x03\x08')
		print("Connected")

		svc = self.getServiceByUUID(UUID_SVC_MIBAND2)
		self.char_auth = svc.getCharacteristics(UUID_CHAR_AUTH)[0]
		self.cccd_auth = self.char_auth.getDescriptors(forUUID=CCCD_UUID)[0]

		svc = self.getServiceByUUID(UUID_SVC_ALERT)
		self.char_alert = svc.getCharacteristics(UUID_CHAR_ALERT)[0]


		svc = self.getServiceByUUID(UUID_SVC_HEART_RATE)
		self.char_hrm_ctrl = svc.getCharacteristics(UUID_CHAR_HRM_CONTROL)[0]
		self.char_hrm = svc.getCharacteristics(UUID_CHAR_HRM_MEASURE)[0]
		self.cccd_hrm = self.char_hrm.getDescriptors(forUUID=CCCD_UUID)[0]

		self.timeout = 5.0
		self.state = None
		# Enable auth service notifications on startup
		self.auth_notif(True)
		self.waitForNotifications(0.1) # Let Band settle


	def init_after_auth(self):
		self.cccd_hrm.write(b"\x01\x00", True)


	def encrypt(self, message):
		aes = AES.new(self._KEY, AES.MODE_ECB)
		return aes.encrypt(message)


	def auth_notif(self, status):
		if status:
			print("Enabling Auth Service notifications status...")
			self.cccd_auth.write(b"\x01\x00", True)
		elif not status:
			print("Disabling Auth Service notifications status...")
			self.cccd_auth.write(b"\x00\x00", True)
		else:
			print("Something went wrong while changing the Auth Service notifications status...")


	def send_key(self):
		print("Sending Key...")
		self.char_auth.write(self._send_key_cmd)
		self.waitForNotifications(self.timeout)


	def req_rdn(self):
		print("Requesting random number...")
		self.char_auth.write(self._send_rnd_cmd)
		self.waitForNotifications(self.timeout)


	def send_enc_rdn(self, data):
		print("Sending encrypted random number")
		cmd = self._send_enc_key + self.encrypt(data)
		send_cmd = struct.pack('<18s', cmd)
		self.char_auth.write(send_cmd)
		self.waitForNotifications(self.timeout)


	def initialize(self):
		self.setDelegate(AuthenticationDelegate(self))
		self.send_key()

		while True:
			self.waitForNotifications(0.1)
			if self.state == "AUTHENTICATED":
				return True
			elif self.state:
				return False


	def authenticate(self):
		self.setDelegate(AuthenticationDelegate(self))
		self.req_rdn()

		while True:
			self.waitForNotifications(0.1)
			if self.state == "AUTHENTICATED":
				return True
			elif self.state:
				return False


	def hrmStartContinuous(self):
		self.char_hrm_ctrl.write(b'\x15\x01\x01', True)


	def hrmStopContinuous(self):
		self.char_hrm_ctrl.write(b'\x15\x01\x00', True)


	def get_device_name(self):
		svc = self.getServiceByUUID("00001800-0000-1000-8000-00805f9b34fb")
		char = svc.getCharacteristics("00002a00-0000-1000-8000-00805f9b34fb")[0]
		data = char.read()
		return data.decode('utf-8')


	def get_sw_revision(self):
		svc = self.getServiceByUUID(UUID_SERVICE_DEVICE_INFO)
		char = svc.getCharacteristics(UUID_CHARACTERISTIC_SW_REVISION)[0]
		data = char.read()
		return data.decode('utf-8')


	def get_hw_revision(self):
		svc = self.getServiceByUUID(UUID_SERVICE_DEVICE_INFO)
		char = svc.getCharacteristics(UUID_CHARACTERISTIC_HRDW_REVISION)[0]
		data = char.read()
		return data.decode('utf-8')


	def get_serial(self):
		svc = self.getServiceByUUID(UUID_SERVICE_DEVICE_INFO)
		char = svc.getCharacteristics(UUID_CHARACTERISTIC_SERIAL)[0]
		data = char.read()
		return data.decode('utf-8')


	def get_current_time(self):
		svc = self.getServiceByUUID(UUID_SVC_MIBAND1)
		char = svc.getCharacteristics(UUID_CHARACTERISTIC_CURRENT_TIME)[0]
		return self._parse_date(char.read()[0:9])


	def _parse_date(self, bytes):
		year = struct.unpack('h', bytes[0:2])[0] if len(bytes) >= 2 else None
		month = bytes[2] if len(bytes) >= 3 else None
		day = bytes[3] if len(bytes) >= 4 else None
		hours = bytes[4] if len(bytes) >= 5 else None
		minutes = bytes[5] if len(bytes) >= 6 else None
		seconds = bytes[6] if len(bytes) >= 7 else None
		day_of_week = bytes[7] if len(bytes) >= 8 else None
		fractions256 = bytes[8] if len(bytes) >= 9 else None

		return {"date": datetime(*(year, month, day, hours, minutes, seconds)), "day_of_week": day_of_week, "fractions256": fractions256}


	def get_battery_info(self):
		svc = self.getServiceByUUID(UUID_SVC_MIBAND1)
		UUID_CHARACTERISTIC_BATTERY = "00000006-0000-3512-2118-0009af100700"
		char = svc.getCharacteristics(UUID_CHARACTERISTIC_BATTERY)[0]
		return self._parse_battery_response(char.read())


	def write_chuncked(self, type, data):
		MAX_CHUNKLENGTH = 23 -6
		pkg_count = 0
		send_data = 0

		svc = self.getServiceByUUID(UUID_SVC_MIBAND1)
		char = svc.getCharacteristics("00000020-0000-3512-2118-0009af100700")[0]
		while send_data < len(data):
			flags=0
			if MAX_CHUNKLENGTH >= len(data) - send_data:
				flags += 128
				if pkg_count == 0:
					flags += 64
			#else:	
			elif pkg_count > 0:
				flags += 64

			new_block_size = min(MAX_CHUNKLENGTH, len(data) - send_data)
			write_val = bytes([0])+ bytes([flags]) + bytes([pkg_count]) + data[send_data:(send_data + new_block_size)]
			#char.write(write_val, withResponse=True)
			char.write(write_val)
			send_data = send_data + new_block_size
			pkg_count += 1


	def _parse_battery_response(self, bytes):
		level = bytes[1] if len(bytes) >= 2 else None
		last_level = bytes[19] if len(bytes) >= 20 else None
		status = 'normal' if bytes[2] == 0 else "charging"
		datetime_last_charge = self._parse_date(bytes[11:18])
		datetime_last_off = self._parse_date(bytes[3:10])

		res = {
			"status": status,
			"level": level,
			"last_level": last_level,
			"last_level": last_level,
			"last_charge": datetime_last_charge,
			"last_off": datetime_last_off
		}
		return res


	def get_steps(self):
		svc = self.getServiceByUUID(UUID_SVC_MIBAND1)
		char = svc.getCharacteristics(UUID_CHARACTERISTIC_STEPS)[0]
		a = char.read()
		steps = struct.unpack('h', a[1:3])[0] if len(a) >= 3 else None
		meters = struct.unpack('h', a[5:7])[0] if len(a) >= 7 else None
		fat_gramms = struct.unpack('h', a[2:4])[0] if len(a) >= 4 else None
		# why only 1 byte??
		callories = a[9] if len(a) >= 10 else None
		return {
			"steps": steps,
			"meters": meters,
			"fat_gramms": fat_gramms,
			"callories": callories
		}


	def set_datetime(self):
		current_datetime = datetime.now()
		day = current_datetime.day
		month = current_datetime.month
		year = current_datetime.year
		hour = current_datetime.hour
		minute = current_datetime.minute
		seconds = current_datetime.second
		day_of_week = current_datetime.isoweekday()

		fraction = int(year / 256)
		rem = year % 256
		write_val = bytes([rem])+ bytes([fraction])+ bytes([month])+ bytes([day])+ bytes([hour])+ bytes([minute])+ bytes([seconds])+ bytes([day_of_week])+ bytes([0])+ bytes([0])+ bytes([4])
		# year,year,month,dayofmonth,hour,minute,second,dayofweek,0,0,tz

		svc = self.getServiceByUUID(UUID_SVC_MIBAND1)
		char = svc.getCharacteristics(UUID_CHARACTERISTIC_CURRENT_TIME)[0]
		char.write(write_val, withResponse=True)
		return 'Date and time set'


	def set_StepGoal(self, target):
		set_goal = [ target%256, int(target/256)]
		write_val = bytes([16])+ bytes([0])+ bytes([0])+ bytes([set_goal[0]])+bytes([set_goal[1]])+ bytes([0])+ bytes([0])
		svc = self.getServiceByUUID("0000fee0-0000-1000-8000-00805f9b34fb")
		char = svc.getCharacteristics("00000008-0000-3512-2118-0009af100700")[0]
		char.write(write_val, withResponse=True)


	def send_FactoryReset(self):
		svc = self.getServiceByUUID("0000fee0-0000-1000-8000-00805f9b34fb")
		char = svc.getCharacteristics("00000003-0000-3512-2118-0009af100700")[0]
		write_val = bytes([6])+ bytes([11])+ bytes([0])+ bytes([1])
		char.write(write_val)


	def set_Timeformat(self, format24):
		svc = self.getServiceByUUID("0000fee0-0000-1000-8000-00805f9b34fb")
		char = svc.getCharacteristics("00000003-0000-3512-2118-0009af100700")[0]
		if format24:
			write_val = bytes([6])+ bytes([2])+ bytes([0])+ bytes([1]) # 24
		else:
			write_val = bytes([6])+ bytes([2])+ bytes([0])+ bytes([0]) # AM/PM
		char.write(write_val)


	def set_Activate_Backlight_on_lift(self, activate, StartHour = 0, StartMinute = 0, EndHour = 0, EndMinute = 0):
		svc = self.getServiceByUUID("0000fee0-0000-1000-8000-00805f9b34fb")
		char = svc.getCharacteristics("00000003-0000-3512-2118-0009af100700")[0]
		if activate:
			# Set enabled
			write_val = bytes([6])+ bytes([5])+ bytes([0])+ bytes([1]) # Enable

			# Set Time if relevant
			if StartHour != EndHour or StartMinute != EndMinute: 
				write_val = write_val + cbytes([StartHour])+ bytes([StartMinute])+ bytes([EndHour])+ bytes([EndMinute])
		else:
			# Set disabled
			write_val = bytes([6])+ bytes([5])+ bytes([0])+ bytes([0]) # Disable

		char.write(write_val)


	def set_MenuOptions(self, elements):
		# Amazfit Bip Watch: Clock, Status, Activity, Weather, Alarm, Timer, Compass, Settings / AliPay
		# MI Band 2: Clock, Steps, Distance, Calories, HeartRate, Battery
		type = self.get_device_name()
		if type == "Amazfit Bip Watch":
			menu_value = [0, 16] # AmazfitBip
		else:
			menu_value = [0, 0] # MiBand2

		element_nr = 0
		elem_block = 0
		for entry in elements:
			if elements[element_nr]:
				menu_value[elem_block] = menu_value[elem_block] + 2**(element_nr - 8*elem_block)

			element_nr = element_nr + 1

			# Switch to next block if blocklimit is reached
			if element_nr%8 == 0:
				elem_block = elem_block + 1


		svc = self.getServiceByUUID(UUID_SVC_MIBAND1)
		char = svc.getCharacteristics("00000003-0000-3512-2118-0009af100700")[0]
		if type == "Amazfit Bip Watch":
			write_val = bytes([10])+ bytes([menu_value[0]])+ bytes([menu_value[1]])+ bytes([0])+ bytes([1])+ bytes([2])+ bytes([3])+ bytes([4])+ bytes([5])+ bytes([6])+ bytes([7])+ bytes([8])
		else:
			write_val = bytes([10])+ bytes([menu_value[0]])+ bytes([menu_value[1]])+ bytes([0])+ bytes([1])+ bytes([2])+ bytes([3])+ bytes([4])+ bytes([5]) #MiBand2

		char.write(write_val)
		#char.write(write_val, withResponse=True)



	#set_alarm(5, True, 6, 27, (1+2+4+8+16))
	def set_alarm(self, position, enabled_bool, hour, minute, repeat):
		if enabled_bool:
			enabled = 128
		else:
			enabled = 0

		#position = 5 # MiBand2: 0-2, Bip: 0-9
		#repeat = 1+2+4+8+16 # 0...Once, 1...Mo, 2...Di, 4...Mi, 8...Do, 16...Fr, 32...Sa, 64...So, 128...Tomorrow
		write_val = bytes([2])+ bytes([enabled + position])+ bytes([hour])+ bytes([minute])+ bytes([repeat])

		UUID_CHARACTERISTIC_CONTROL_POINT = '00000003-0000-3512-2118-0009af100700'
		svc = self.getServiceByUUID(UUID_SVC_MIBAND1)
		char = svc.getCharacteristics(UUID_CHARACTERISTIC_CONTROL_POINT)[0]
		char.write(write_val)
		#char.write(write_val, withResponse=True)
		return 'Alarm set'


	def send_Notification(self, type): 
		AlertCategory = {
			"SMS": 1,
			"CALL": 2,
			"OFF": 0,
			"TST": -1,
		}

		if type == "TST":
			for x in range(3, 256):
				print (x)
				self.char_alert.write(bytes([x]))
				time.sleep(3)
		else:
			self.char_alert.write(bytes([AlertCategory[type]]))
			return "Sending "+type+" notification..."


	def send_Text_Notification(self, type, subject, message):
		#AlertCategory { Simple(0), Email(1), News(2), IncomingCall(3), MissedCall(4), SMS(5), VoiceMail(6), Schedule(7), HighPriorityAlert(8), InstantMessage(9), // 10-250 reserved for future use // 251-255 defined by service specification Any(255), Custom(-1), CustomHuami(-6);
		AlertCategory = {
			"E-MAIL": 1,
			"MISSED_CALL": 4,
			"SMS": 5,
		}

		device_type = self.get_device_name()
		if device_type == "Amazfit Bip Watch" or device_type == "Mi Band 3":
			header = bytes([1])
		else:
			header = bytes([0])+ bytes([0])+ bytes([0])+ bytes([0])+ bytes([1])

		write_val = bytes([AlertCategory[type]])+ header + subject.encode('utf-8') + bytes([0])+ message.encode('utf-8') + bytes([0])+ bytes([0])
		self.write_chuncked(0, write_val)
		return 'Notification send'




class AuthenticationDelegate(DefaultDelegate):

	"""This Class inherits DefaultDelegate to handle the authentication process."""
	def __init__(self, device):
		DefaultDelegate.__init__(self)
		self.device = device


	def handleNotification(self, hnd, data):
		# Debug purposes
		#print("HANDLE: " + str(hex(hnd)))
		#print("DATA: " + str(data.encode("hex")))
		if hnd == self.device.char_auth.getHandle():
			if data[:3] == b'\x10\x01\x01':
				self.device.req_rdn()
			elif data[:3] == b'\x10\x01\x04':
				self.device.state = "ERROR: Key Sending failed"
			elif data[:3] == b'\x10\x02\x01':
				random_nr = data[3:]
				self.device.send_enc_rdn(random_nr)
			elif data[:3] == b'\x10\x02\x04':
				self.device.state = "ERROR: Something wrong when requesting the random number..."
			elif data[:3] == b'\x10\x03\x01':
				print("Authenticated!")
				self.device.state = "AUTHENTICATED"
			elif data[:3] == b'\x10\x03\x04':
				print("Encryption Key Auth Fail, sending new key...")
				self.device.send_key()
			else:
				self.device.state = "ERROR: Auth failed"
			#print("Auth Response: " + str(data.encode("hex")))
		elif hnd == self.device.char_hrm.getHandle():
			rate = struct.unpack('bb', data)[1]
			print("Heart Rate: " + str(rate))
		else:
			print("Unhandled Response " + hex(hnd) + ": " + str(data.encode("hex")))


def main():
	""" main func """
	parser = argparse.ArgumentParser()
	parser.add_argument('host', action='store', help='MAC of BT device')
	parser.add_argument('-key', '--device_key', action='store', default='', help='Secret Device Key')
	parser.add_argument('-t', action='store', type=float, default=3.0,
                        help='duration of each notification')

	parser.add_argument('--init', action='store_true', default=False)
	parser.add_argument('-i', '--info', action='store_true', default=False)
	parser.add_argument('-fr', '--factory_reset', action='store_true', default=False)
	parser.add_argument('-n', '--notify', action='store_true', default=False)
	parser.add_argument('-dt', '--set_datetime', action='store_true', default=False)
	parser.add_argument('-tf', '--time_format', action='store', default="", 
                        help='Set Time format to 12-hour or 24-hour')
	parser.add_argument('-hrm', '--heart', action='store_true', default=False)
	parser.add_argument('-sg', '--step-goal', action='store', type=int, default=0, 
                        help='Set Step Goal')
	parser.add_argument('-bl', '--backlight_on_lift', action='store', default="", 
                        help='Activate backlight on lift')
	parser.add_argument('-tst', '--test', action='store_true', default=False)
	arg = parser.parse_args(sys.argv[1:])

	print('Connecting to ' + arg.host)
	band = MiBand2(arg.host, arg.device_key)
	band.setSecurityLevel(level="medium")

	if arg.init:
		if band.initialize():
			print("Init OK")
		band.disconnect()
		return
	else:
		band.authenticate()

	band.init_after_auth()

	if arg.test:
		pass

		#band.set_alarm(0, False, 13, 15, (1+2+4+8+16))
		#band.set_MenuOptions([True, False, False, False, False, False, False, False, True]) 
		#band.set_MenuOptions([True, True, True, True, True, True, True, True, False]) 

		#band.set_MenuOptions([True, False, False, False, False, False]) 
		#band.set_MenuOptions([True, True, True, True, True, True]) 

		#band.send_Text_Notification("SMS", "From myself", "Hello this is a test!")
		#band.send_Text_Notification("E-MAIL", "From myself", "Hello this is a test!")
		#band.send_Text_Notification("MISSED_CALL", "From myself", "Hello this is a test!")

		#band.send_Notification("CALL")

	if arg.info:
		print ("Device Name: ", band.get_device_name())
		print ("SW Version: ", band.get_sw_revision())
		print ("HW Version: ", band.get_hw_revision())
		print ("Serial: ", band.get_serial())
		print ("Time: ", band.get_current_time())
		print ("Battery: ", band.get_battery_info())
		print ("Steps: ", band.get_steps())

	if arg.factory_reset:
		band.send_FactoryReset()

	if arg.set_datetime:
		print (band.set_datetime())
		band.set_Timeformat(True)

	if arg.time_format != "":
		if arg.time_format == "24":
			band.set_Timeformat(True)
		elif arg.time_format == "12":
			band.set_Timeformat(False)

	if arg.notify:
		print (band.send_Notification("SMS"))
		time.sleep(arg.t)
		print (band.send_Notification("CALL"))
		time.sleep(arg.t)
		band.send_Notification("OFF")
		#band.send_Notification("TST")

	if arg.step_goal > 0:
		band.set_StepGoal( arg.step_goal )

	if arg.backlight_on_lift != "":
		if arg.backlight_on_lift.lower() == "active" or arg.backlight_on_lift.lower() == "on":
			band.set_Activate_Backlight_on_lift(True)
		elif arg.backlight_on_lift.lower() == "inactive" or arg.backlight_on_lift.lower() == "off":
			band.set_Activate_Backlight_on_lift(False)
		#band.set_Activate_Backlight_on_lift(True, 2, 0, 23, 59)

	if arg.heart:
		print("Cont. HRM start")
		band.hrmStopContinuous()
		band.hrmStartContinuous()
		for i in range(30):
			band.waitForNotifications(1.0)
	band.hrmStopContinuous()


	print("Disconnecting...")
	band.disconnect()
	del band


if __name__ == "__main__":
	main()
