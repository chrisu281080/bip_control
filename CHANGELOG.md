## V0.6 (2021-01-10)
- Enabled text notifications for Amazfit GTR 2

## V0.5 (2021-01-06)
- Fixed handling of device-keys
- Automatic handling of RANDOM vs PUBLIC Bluetooth Address
- Tested with Amazfit GTR 2

## V0.4 (2020-11-22)
- Switched to python3 incl. fixes for compatibility

## V0.3 (2020-09-18)
- Added compatibiliy for MiBand3
- Added compatibiliy for MiBand4

## V0.2 (2020-08-18)
- Added get_device_name (LIB, CLI & GUI)
- Added set_Timeformat (LIB, CLI & GUI)
- Added set_Activate_Backlight_on_lift (LIB, CLI & GUI)
- Added send_FactoryReset (LIB, CLI & GUI)
- Added set_MenuOptions (LIB & GUI)
- Added set_StepGoal (LIB, CLI & GUI)

## V0.1 (2020-08-03)
- Initial release
