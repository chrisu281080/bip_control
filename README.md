# bip_control

This a script to control an Amazfit BIP (should be also be mostly compatible with Mi Band 2 & 3). In addition to the CLI "bip_control.py" it includes a compact GUI "bip_control_gui" which is designed to be used for the librem5 (or other Linux Smartphones with GTK). But I can also be used on normal screens.

Thx to all the existing python scripts for MiBand2 and especially GadgetBridge which have been important resources for this project.


## Before using bip_control
apt install python3-pip libglib2.0-dev

pip3 install bluepy

Do identify your Bluetooth adresse of your Amazfit BIP you should run:

sudo hcitool lescan


## Before using bip_control_gui
If you want to use the GUI you should also install the recommended "python-gtk2":

apt install python-gtk2


## Special Notes - Device Key
This can be left blank for AmazfitBip, MiBand2 & MiBand3.

It is required for MiBand4 and needs to be extracted first. (see GadgetBridge for details)


## Currently supported features
| GUI Tab | Option | Function | AmazfitBip | MiBand2 | MiBand3 | MiBand4 | Amazfit GTR2 |
| --- | -------- | -------- | ---------- | ------- | ------- | ------- | ------- |
| Connect | Init |  | works | works | n/a | n/a | n/a | 
| Info | Read Device Name | band.get_device_name() | works | works | works | works |  works | 
| Info | Read Time | band.get_current_time() | works | works | works | works | works | 
| Info | Read Battery | band.get_battery_info() | works | works | works | works | works | 
| Info | Serial | band.get_serial() | works | works | works | works | works | 
| Info | HW Version | band.get_hw_revision() | works | works | works | works | works | 
| Info | SW Version | band.get_sw_revision() | works | works | works | works | works | 
| Settings | Set Timeformat 12h/24h | band.set_Timeformat(True) | works | works | works | works | works | 
| Settings | Set Activate Backlight on lift | band.set_Activate_Backlight_on_lift(True) | works | works | works | works | not yet |
| Settings | Factory Reset | band.send_FactoryReset() | works | works | works | works | not tested |
| Settings | Configure Watch Menu | band.set_MenuOptions([True, False, ...]) | works | works | not yet | not yet | not yet | 
| Alarms | Configure Alarms |  band.set_alarm() | works | works | works | works | works | 
| Alarms | Time Sync |  band.set_datetime() | works | works | works | works | works | 
| Notification | Text Notifications | band.send_Text_Notification("SMS", "From myself", "Hello this is a test!") | works | n/a | works | works | works |
| Fitness | Read Steps | band.get_steps()) | works | works | works | works | works | 
| Fitness | Set Steps Goal | band.set_StepGoal(8000) | works | works | works | works | not yet | 
| Fitness | HeartRate Measurement |   | works | works | works | works | works | 
| No GUI | Message |  band.send_Notification("SMS") | works | works | n/a | n/a | n/a | 
| No GUI | Phone |  band.send_Notification("CALL") | works | works | n/a | n/a | n/a | 
| No GUI | Stop |  band.send_Notification("OFF") | works | works | n/a | n/a | n/a | 
| No GUI | Test |  band.send_Notification("TST") | works | works | n/a | n/a | n/a | 


## License
### GNU GPL v3.0

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)
